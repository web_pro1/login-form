export default interface User {
  id: number;
  login: String;
  name: string;
  password: string;
}
